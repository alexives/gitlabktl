package knative

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHasCredentials(t *testing.T) {
	t.Run("when credentials are defined", func(t *testing.T) {
		registry := Registry{
			Username: "test-user",
			Password: "test-pass",
		}

		assert.True(t, registry.HasCredentials())
	})

	t.Run("when credentials are missing", func(t *testing.T) {
		registry := Registry{}

		assert.False(t, registry.HasCredentials())
	})

	t.Run("when credentials are blank", func(t *testing.T) {
		registry := Registry{Username: "", Password: ""}

		assert.False(t, registry.HasCredentials())
	})
}
