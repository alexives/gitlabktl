package knative

import (
	"errors"
	"fmt"

	"github.com/triggermesh/tm/pkg/client"
	"github.com/triggermesh/tm/pkg/resources/credential"
	tmservicepkg "github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

func init() {
	setDefaultClusterFactory()
}

var clusterFactory func(config Config) (Cluster, error)

type Cluster interface {
	SetDryRun()
	SetNoDryRun()
	Namespace() string
	DeployRegistryCredentials() error
	DeployService(service Service) (string, error)
	DeployServices(services []Service) (string, error)
}

func NewCluster(config Config) (Cluster, error) {
	return clusterFactory(config)
}

func setDefaultClusterFactory() {
	clusterFactory = func(config Config) (Cluster, error) {
		return newTriggermeshCluster(config)
	}
}

// Triggermesh represent a Triggermesh cluster that we want to interface with
// using Triggermesh's `tm` tool.
type tmCluster struct {
	clientset     *client.ConfigSet
	registry      Registry
	knativeClient KnativeClient
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) SetDryRun() {
	logger.Debug("setting a cluster dry-run mode")

	client.Dry = true
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) SetNoDryRun() {
	logger.Debug("setting a real cluster interactions mode")

	client.Dry = false
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) Namespace() string {
	return client.Namespace
}

func (cluster *tmCluster) DeployRegistryCredentials() error {
	if !cluster.registry.HasCredentials() {
		return errors.New("missing registry credentials")
	}

	logger.Info("deploying registry credentials")

	tmCreds := credential.RegistryCreds{
		Name:      "gitlab-registry",
		Namespace: client.Namespace,
		Host:      cluster.registry.Host,
		Username:  cluster.registry.Username,
		Password:  cluster.registry.Password,
		Pull:      true,
	}

	return tmCreds.CreateRegistryCreds(cluster.clientset)
}

func (cluster *tmCluster) DeployService(service Service) (string, error) {
	logger.Info("deploying a service to a cluster")

	tmservice, err := newTriggermeshService(service, cluster)
	if err != nil {
		return "deployment failed", fmt.Errorf("could not define a service: %w", err)
	}

	compatible, err := cluster.knativeClient.isCompatible()
	if !compatible && err != nil {
		return "", err
	}

	return tmservice.Deploy(cluster.clientset)
}

func (cluster *tmCluster) DeployServices(services []Service) (string, error) {
	var functions []tmservicepkg.Service

	for _, service := range services {
		tmservice, err := newTriggermeshService(service, cluster)
		if err != nil {
			return "deployment error", fmt.Errorf("could not build deployable service: %w", err)
		}

		functions = append(functions, tmservice.Service)
	}

	deployer, err := newTriggermeshService(Service{}, cluster)
	if err != nil {
		return "", err
	}

	compatible, err := cluster.knativeClient.isCompatible()
	if !compatible && err != nil {
		return "", err
	}

	return "", deployer.DeployFunctions(functions, true, 3, cluster.clientset)
}

func newTriggermeshCluster(config Config) (*tmCluster, error) {
	client.Namespace = config.Namespace()
	client.Wait = true

	clientset, err := client.NewClient(config.KubeConfig())
	if err != nil {
		fmt.Errorf("could not read cluster config: %w", err)
	}

	knativeClient, err := NewClient(config.KubeConfig())
	if err != nil {
		fmt.Errorf("could not initialize knative client: %w", err)
	}

	return &tmCluster{clientset: &clientset, registry: config.Registry(), knativeClient: knativeClient}, nil
}
