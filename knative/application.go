package knative

import (
	"context"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

// Application representa a Knative application that can be deployed to a
// cluster.
type Application struct {
	service  Service
	cluster  Cluster
	registry Registry
}

func (app *Application) DeployDryRun() (string, error) {
	logger.Info("running application dry-run deployment")

	app.cluster.SetDryRun()

	return app.cluster.DeployService(app.service)
}

func (app *Application) Deploy(ctx context.Context) (string, error) {
	logger.Info("running application deployment")

	app.cluster.SetNoDryRun()

	if app.registry.HasCredentials() {
		app.cluster.DeployRegistryCredentials()
	}

	return app.cluster.DeployService(app.service)
}
