package knative

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	testclient "k8s.io/client-go/kubernetes/fake"
)

func TestKnativeClientGetVersion(t *testing.T) {
	t.Run("returns the version if knative appears installed", func(t *testing.T) {
		fakeClient := testclient.NewSimpleClientset()
		fakeClient.AppsV1().Deployments("knative-serving").Create(&v1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name: "controller",
				Labels: map[string]string{
					"serving.knative.dev/release": "v0.7.0",
				},
			},
		})
		client := &Client{k8s: fakeClient}

		version, err := client.getVersion()

		assert.Equal(t, version, "v0.7.0", "should have returned version == v0.7.0")
		require.NoError(t, err)
	})

	t.Run("treats 'devel' as 'v0.5.0' when returned version is more than the range", func(t *testing.T) {
		fakeClient := testclient.NewSimpleClientset()
		fakeClient.AppsV1().Deployments("knative-serving").Create(&v1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name: "controller",
				Labels: map[string]string{
					"serving.knative.dev/release": "devel",
				},
			},
		})
		client := &Client{k8s: fakeClient}

		compatible, err := client.getVersion()

		assert.Equal(t, compatible, "v0.5.0", "for versions called devel, it should return v0.5.0")
		require.NoError(t, err)
	})

	t.Run("returns an error if knative looks not installed", func(t *testing.T) {
		client := &Client{k8s: testclient.NewSimpleClientset()}

		version, err := client.getVersion()

		assert.Equal(t, version, "", "should have returned installed as empty string")
		require.EqualError(t, err, "knative may not be installed, please install knative")
	})
}

func TestKnativeClientIsCompatible(t *testing.T) {
	t.Run("returns true when returned version is in range", func(t *testing.T) {
		fakeClient := testclient.NewSimpleClientset()
		fakeClient.AppsV1().Deployments("knative-serving").Create(&v1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name: "controller",
				Labels: map[string]string{
					"serving.knative.dev/release": "v0.7.0",
				},
			},
		})
		client := &Client{k8s: fakeClient}

		compatible, err := client.isCompatible()

		assert.True(t, compatible, "should have returned compatible == true")
		require.NoError(t, err)
	})

	t.Run("returns false when returned version is less than the range", func(t *testing.T) {
		fakeClient := testclient.NewSimpleClientset()
		fakeClient.AppsV1().Deployments("knative-serving").Create(&v1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name: "controller",
				Labels: map[string]string{
					"serving.knative.dev/release": "v0.2.0",
				},
			},
		})
		client := &Client{k8s: fakeClient}

		compatible, err := client.isCompatible()

		assert.False(t, compatible, "should have returned compatible == false")
		require.EqualError(t, err, "knative version out of range, must have knative "+VersionConstraint)
	})

	t.Run("returns false when returned version is more than the range", func(t *testing.T) {
		fakeClient := testclient.NewSimpleClientset()
		fakeClient.AppsV1().Deployments("knative-serving").Create(&v1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name: "controller",
				Labels: map[string]string{
					"serving.knative.dev/release": "v9000.0.0",
				},
			},
		})
		client := &Client{k8s: fakeClient}

		compatible, err := client.isCompatible()

		assert.False(t, compatible, "should have returned compatible == false")
		require.EqualError(t, err, "knative version out of range, must have knative "+VersionConstraint)
	})
}
