package knative

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTmClusterServiceDeployment(t *testing.T) {
	WithMockClient(&MockClient{Compatible: true}, func(client *MockClient) {
		config := new(MockConfig)
		config.On("Namespace").Return("config-namespace").Once()
		config.On("KubeConfig").Return("../k8s/testdata/kubeconfig.json").Twice()
		config.On("Registry").Return(Registry{}).Once()
		defer config.AssertExpectations(t)

		cluster, err := newTriggermeshCluster(config)
		require.NoError(t, err)

		cluster.SetDryRun()

		t.Run("when service namespace is not defined", func(t *testing.T) {
			service := Service{
				Name:    "my-service",
				Image:   "registry.gitlab.com/my/source",
				Secrets: []string{"my-secret"},
			}

			summary, err := cluster.DeployService(service)
			require.NoError(t, err)

			assert.Contains(t, summary, `"kind": "Service"`)
			assert.Contains(t, summary, `"apiVersion": "serving.knative.dev/v1alpha1"`)
			assert.Contains(t, summary, `"image": "registry.gitlab.com/my/source"`)
			assert.Contains(t, summary, `"name": "my-secret"`)
			assert.Contains(t, summary, `"namespace": "config-namespace"`)
		})

		t.Run("when service namespace is defined", func(t *testing.T) {
			service := Service{
				Name:      "my-service",
				Namespace: "my-namespace",
				Image:     "registry.gitlab.com/my/source",
				Secrets:   []string{"my-secret"},
			}

			summary, err := cluster.DeployService(service)
			require.NoError(t, err)

			assert.Contains(t, summary, `"kind": "Service"`)
			assert.Contains(t, summary, `"apiVersion": "serving.knative.dev/v1alpha1"`)
			assert.Contains(t, summary, `"image": "registry.gitlab.com/my/source"`)
			assert.Contains(t, summary, `"name": "my-secret"`)
			assert.Contains(t, summary, `"namespace": "my-namespace"`)
		})
	})
}

func TestTmClusterMissingCredentialsDeployment(t *testing.T) {
	WithMockClient(&MockClient{Compatible: true}, func(client *MockClient) {
		config := new(MockConfig)
		config.On("Namespace").Return("config-namespace").Once()
		config.On("KubeConfig").Return("../k8s/testdata/kubeconfig.json").Twice()
		config.On("Registry").Return(Registry{}).Once()
		defer config.AssertExpectations(t)

		cluster, err := newTriggermeshCluster(config)
		require.NoError(t, err)

		err = cluster.DeployRegistryCredentials()

		assert.EqualError(t, err, "missing registry credentials")
	})
}
