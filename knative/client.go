package knative

import (
	"errors"
	"fmt"

	semver "github.com/Masterminds/semver"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

const VersionConstraint = "0.5 - 0.9"

func init() {
	setDefaultClientFactory()
}

var clientFactory func(configPath string) (KnativeClient, error)

type KnativeClient interface {
	isCompatible() (bool, error)
}

// Used for communicating with knative/kubernetes
type Client struct {
	k8s kubernetes.Interface
}

func NewClient(configPath string) (KnativeClient, error) {
	return clientFactory(configPath)
}

func setDefaultClientFactory() {
	clientFactory = func(configPath string) (KnativeClient, error) {
		return newKnativeClient(configPath)
	}
}

func (client *Client) getVersion() (string, error) {
	var deployments, err = client.k8s.AppsV1().Deployments("knative-serving").List(metav1.ListOptions{})
	if err != nil {
		return "", fmt.Errorf("encoutered an error while checking for knative installation: %w", err)
	}

	for item := range deployments.Items {
		if deployments.Items[item].GetName() == "controller" {
			version := deployments.Items[item].GetLabels()["serving.knative.dev/release"]

			// This is special handling for the earliest versions deployed to gitlab managed
			// clusters. They were version 0.5.0, but had
			if version == "devel" {
				return "v0.5.0", nil
			}
			return version, nil
		}
	}

	logger.Warn("could not detect knative installation; if it's already installed give the service account access to get deployments in the knative-serving namespace. See: https://docs.gitlab.com/ee/user/project/clusters/serverless/#using-an-existing-installation-of-knative")
	return "", errors.New("knative may not be installed, please install knative")
}

func (client *Client) isCompatible() (bool, error) {
	versionString, err := client.getVersion()
	if err != nil {
		return false, err
	}

	version, err := semver.NewVersion(versionString)
	if err != nil {
		return false, fmt.Errorf("version from server not parseable, knative may not be installed or is incompatible, or the deployment labels are corrupt: %s", err)
	}

	constraint, err := semver.NewConstraint(VersionConstraint)
	if err != nil {
		return false, fmt.Errorf("versionConstraint was not parseable, please fix: %s", err)
	}

	if !constraint.Check(version) {
		return false, errors.New("knative version out of range, must have knative " + VersionConstraint)
	}

	return true, nil
}

func newKnativeClient(configPath string) (*Client, error) {
	config, cfgError := clientcmd.BuildConfigFromFlags("", configPath)
	if cfgError != nil {
		return nil, cfgError
	}

	k8s, clientError := kubernetes.NewForConfig(config)
	if cfgError != nil {
		return nil, clientError
	}

	return &Client{k8s: k8s}, nil
}
