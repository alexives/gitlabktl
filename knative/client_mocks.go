package knative

import (
	"github.com/stretchr/testify/mock"
)

type MockClient struct {
	mock.Mock
	Error      error
	Compatible bool
}

func (mock *MockClient) isCompatible() (bool, error) {
	if mock.Compatible != false {
		return mock.Compatible, nil
	}
	return mock.Compatible, mock.Error
}

func WithMockClient(mock *MockClient, block func(mock *MockClient)) {
	defer setDefaultClusterFactory()

	clientFactory = func(configPath string) (KnativeClient, error) {
		return mock, nil
	}

	block(mock)
}
