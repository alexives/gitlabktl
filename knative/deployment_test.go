package knative

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewApplicationDeployer(t *testing.T) {
	WithMockClient(&MockClient{Compatible: true}, func(client *MockClient) {
		service := Service{Name: "my-app"}
		config := new(MockConfig)

		config.On("Namespace").Return("my-namespace").Once()
		config.On("KubeConfig").Return("my/.kubeconfig").Twice()
		config.On("Registry").Return(Registry{}).Twice()
		defer config.AssertExpectations(t)

		deployer, err := NewApplicationDeployer(service, config)
		require.NoError(t, err)

		assert.Equal(t, deployer.(*Application).service.Name, "my-app")
	})
}
