package kaniko

import (
	"context"

	"github.com/stretchr/testify/mock"
)

type MockBuilder struct {
	mock.Mock
}

func (client *MockBuilder) Build(ctx context.Context) error {
	args := client.Called()

	return args.Error(0)
}

func (client *MockBuilder) BuildDryRun() (string, error) {
	args := client.Called()

	return args.String(0), args.Error(1)
}
