package builder

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/builder/docker"
	"gitlab.com/gitlab-org/gitlabktl/builder/kaniko"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

func TestNewFromRuntime(t *testing.T) {
	t.Run("when only Kaniko is available", func(t *testing.T) {
		runtime := new(runtime.MockRuntime)

		runtime.On("Directory").Return("my/func").Once()
		runtime.On("Image").Return("my/image:latest").Once()
		runtime.On("DockerfilePath").Return("my/Dockerfile.test").Once()

		config := env.Stubs{
			"CI_REGISTRY":          "registry.test",
			"CI_REGISTRY_USER":     "my-user",
			"CI_REGISTRY_PASSWORD": "my-password",
		}

		env.WithStubbedEnv(config, func() {
			kaniko.WithKanikoAvailable(func() {
				builder, err := NewFromRuntime(runtime)
				require.NoError(t, err)

				kanikoBuilder := builder.(*kaniko.Kaniko)

				assert.Equal(t, kanikoBuilder.Before, []kaniko.Builder{runtime})
				assert.Equal(t, kanikoBuilder.Dockerfile, "my/Dockerfile.test")
				assert.Equal(t, kanikoBuilder.Workspace, "my/func")
				assert.Equal(t, kanikoBuilder.Destinations, []string{"my/image:latest"})
				assert.Equal(t, kanikoBuilder.Registry.Host, "registry.test")
				assert.Equal(t, kanikoBuilder.Registry.Credentials, registry.Credentials{Username: "my-user", Password: "my-password"})
			})
		})

		runtime.AssertExpectations(t)
	})

	t.Run("when only docker is available", func(t *testing.T) {
		runtime := new(runtime.MockRuntime)

		runtime.On("Directory").Return("my/func").Once()
		runtime.On("Image").Return("my/image:latest").Once()
		runtime.On("DockerfilePath").Return("my/Dockerfile.test").Once()

		docker.WithDockerAvailable(func() {
			builder, err := NewFromRuntime(runtime)
			require.NoError(t, err)

			dockerBuilder := builder.(*docker.Docker)

			assert.Equal(t, dockerBuilder.Before, []docker.Builder{runtime})
			assert.Equal(t, dockerBuilder.DockerfilePath, "my/Dockerfile.test")
			assert.Equal(t, dockerBuilder.Workspace, "my/func")
			assert.Equal(t, dockerBuilder.Destinations, []string{"my/image:latest"})
		})

		runtime.AssertExpectations(t)
	})
}

func TestNewFromRequest(t *testing.T) {
	t.Run("when only Kaniko is available", func(t *testing.T) {
		config := env.Stubs{
			"CI_REGISTRY":          "registry.test",
			"CI_REGISTRY_USER":     "my-user",
			"CI_REGISTRY_PASSWORD": "my-password",
		}

		env.WithStubbedEnv(config, func() {
			kaniko.WithKanikoAvailable(func() {
				builder, err := NewFromRequest(Request{
					Dockerfile: "my/Dockerfile.test",
					Directory:  "my/func",
					Image: registry.Image{
						Repository: "registry.test/foo",
						Tag:        "latest",
					},
				})
				require.NoError(t, err)

				expectedBuilder := kaniko.Kaniko{
					Dockerfile:   "my/Dockerfile.test",
					Workspace:    "my/func",
					Destinations: []string{"registry.test/foo:latest"},
					Registry: registry.Registry{
						Host: "registry.test",
						Credentials: registry.Credentials{
							Username: "my-user",
							Password: "my-password",
						},
					},
				}

				assert.Equal(t, &expectedBuilder, builder)
			})
		})
	})

	t.Run("when only docker is available", func(t *testing.T) {
		docker.WithDockerAvailable(func() {
			builder, err := NewFromRequest(Request{
				Dockerfile: "my/Dockerfile.test",
				Directory:  "my/func",
				Image:      registry.Image{},
			})
			require.NoError(t, err)

			assert.IsType(t, &docker.Docker{}, builder)
		})
	})
}
