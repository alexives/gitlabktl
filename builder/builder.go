package builder

import (
	"context"
	"errors"

	"gitlab.com/gitlab-org/gitlabktl/builder/docker"
	"gitlab.com/gitlab-org/gitlabktl/builder/kaniko"
	"gitlab.com/gitlab-org/gitlabktl/registry"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

// Request describes what a builder needs to build
type Request struct {
	Dockerfile string
	Directory  string
	Image      registry.Image
}

// Builder interface, currently Kaniko, Docker Engine later.
// A runtime itself is a builder too.
type Builder interface {
	BuildDryRun() (string, error)
	Build(ctx context.Context) error
}

// Create a new Builder from a function runtime
func NewFromRuntime(runtime runtime.Runtime) (Builder, error) {
	if kaniko.IsAvailable() {
		return &kaniko.Kaniko{
			Before:       []kaniko.Builder{runtime},
			Dockerfile:   runtime.DockerfilePath(),
			Workspace:    runtime.Directory(),
			Destinations: []string{runtime.Image()},
			Registry:     registry.NewWithPushAccess(),
		}, nil
	} else if docker.IsAvailable() {
		return docker.NewBuilder(docker.BuildRequest{
			Before:         []docker.Builder{runtime},
			DockerfilePath: runtime.DockerfilePath(),
			Workspace:      runtime.Directory(),
			Destinations:   []string{runtime.Image()},
		})
	}

	return nil, errors.New("neither Kaniko nor Docker could be found")
}

// Create a ne Builder from a Builder Request
func NewFromRequest(request Request) (Builder, error) {
	if kaniko.IsAvailable() {
		return &kaniko.Kaniko{
			Dockerfile:   request.Dockerfile,
			Workspace:    request.Directory,
			Destinations: request.Image.Locations(),
			Registry:     request.Image.PushRegistry(),
		}, nil
	} else if docker.IsAvailable() {
		return docker.NewBuilder(docker.BuildRequest{
			DockerfilePath: request.Dockerfile,
			Workspace:      request.Directory,
			Destinations:   request.Image.Locations(),
		})
	}

	return nil, errors.New("neither Kaniko nor Docker could be found")
}
