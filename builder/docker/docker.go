package docker

import (
	"bytes"
	"context"
	"errors"
	"io"
	"os"
	"path"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/archive"
	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/docker/docker/pkg/term"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/logger"
)

type BuildRequest struct {
	Before         []Builder
	DockerfilePath string
	Workspace      string
	Destinations   []string
}

type Docker struct {
	BuildRequest
	engine Engine
}

type Engine interface {
	Ping(context.Context) (types.Ping, error)
	ImageBuild(context.Context, io.Reader, types.ImageBuildOptions) (types.ImageBuildResponse, error)
}

type Builder interface {
	BuildDryRun() (string, error)
	Build(ctx context.Context) error
}

var isAvailable func() bool

var createDockerEngine = func() (Engine, error) {
	dockerEngine, err := client.NewClientWithOpts(client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}

	err = client.FromEnv(dockerEngine)
	if err != nil {
		return nil, err
	}

	return dockerEngine, nil
}

func init() {
	setDefaultAvailabilityCheck()
}

func setDefaultAvailabilityCheck() {
	isAvailable = func() bool {
		engine, err := createDockerEngine()
		if err != nil {
			return false
		}

		ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(10*time.Second))
		defer cancel()

		_, err = engine.Ping(ctx)
		if err != nil {
			return false
		}

		return true
	}
}

func IsAvailable() bool {
	return isAvailable()
}

func WithDockerAvailable(block func()) {
	isAvailable = func() bool { return true }
	defer setDefaultAvailabilityCheck()

	block()
}

func NewBuilder(request BuildRequest) (Builder, error) {
	dockerEngine, err := createDockerEngine()
	if err != nil {
		return nil, err
	}

	return &Docker{
		BuildRequest: request,
		engine:       dockerEngine,
	}, nil
}

func (docker *Docker) Build(ctx context.Context) error {
	logger.Info("running Docker before builders")

	for _, builder := range docker.Before {
		err := builder.Build(ctx)
		if err != nil {
			return err
		}
	}

	logger.Info("building runtime image using Docker")

	err := docker.requireDockerfile()
	if err != nil {
		return err
	}

	return docker.execute(ctx)
}

func (docker *Docker) BuildDryRun() (string, error) {
	var summary bytes.Buffer

	for _, builder := range docker.Before {
		details, err := builder.BuildDryRun()
		if err != nil {
			return "nested docker before builder error", err
		}

		summary.WriteString("before build: " + details)
	}

	logger.Info("building runtime image using docker (dry-run!)")
	logger.WithField("Dockerfile", docker.DockerfilePath).Info("using Dockerfile")

	// TODO in the future we might want to reuse a runtime BuildDryRun
	// to provide more information about building a runtime
	summary.WriteString("Dockerfile path: " + docker.DockerfilePath + "\n")
	dockerfile, _ := fs.ReadFile(docker.DockerfilePath)
	summary.WriteString("Dockerfile contents:\n" + string(dockerfile) + "\n")

	return summary.String(), nil
}

func (docker *Docker) requireDockerfile() error {
	if exists, _ := fs.Exists(docker.DockerfilePath); !exists {
		logger.WithField("dockerfile", docker.DockerfilePath).Warn("dockerfile not found")
		return errors.New("dockerfile not found")
	}

	return nil
}

func (docker *Docker) execute(ctx context.Context) error {
	engine := docker.engine

	opts := types.ImageBuildOptions{
		Tags:       docker.Destinations,
		Dockerfile: path.Base(docker.DockerfilePath),
	}

	reader, err := archive.Tar(docker.Workspace, archive.Uncompressed)
	if err != nil {
		return err
	}

	res, err := engine.ImageBuild(ctx, reader, opts)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	termFd, isTerm := term.GetFdInfo(os.Stderr)

	return jsonmessage.DisplayJSONMessagesStream(res.Body, os.Stderr, termFd, isTerm, nil)
}
