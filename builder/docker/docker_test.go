package docker

import (
	"context"
	"errors"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func newDocker() Docker {
	return Docker{
		BuildRequest: BuildRequest{
			DockerfilePath: "/home/Dockerfile.tmp",
			Workspace:      "build/",
		},
		engine: new(MockEngine),
	}
}

func TestIsAvailable(t *testing.T) {
	t.Run("when docker engine cannot be reached", func(t *testing.T) {
		createDockerEngine = func() (Engine, error) {
			engine := new(MockEngine)
			//noinspection GoErrorStringFormat
			engine.On("Ping", mock.Anything).Return(types.Ping{},
				errors.New("Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?"))

			return engine, nil
		}

		assert.Equal(t, false, IsAvailable())
	})

	t.Run("when docker engine is available", func(t *testing.T) {
		createDockerEngine = func() (Engine, error) {
			engine := new(MockEngine)
			engine.On("Ping", mock.Anything).Return(types.Ping{}, nil)

			return engine, nil
		}

		assert.Equal(t, true, IsAvailable())
	})
}

func TestBuild(t *testing.T) {
	t.Run("with missing Dockerfile", func(t *testing.T) {
		fs.WithTestFs(func() {
			docker := newDocker()

			err := docker.Build(context.Background())

			assert.Equal(t, "dockerfile not found", err.Error())
		})
	})

	t.Run("when nested builder returns an error", func(t *testing.T) {
		before := new(MockBuilder)
		before.On("Build").Return(errors.New("nested-builder-error")).Once()

		fs.WithTestFs(func() {
			docker := newDocker()
			docker.Before = []Builder{before}

			err := docker.Build(context.Background())

			assert.Equal(t, err.Error(), "nested-builder-error")
		})
	})
}

func TestBuildDryRun(t *testing.T) {
	t.Run("without nested builders", func(t *testing.T) {
		fs.WithTestFs(func() {
			docker := newDocker()

			err := fs.WriteFile(docker.DockerfilePath, []byte("FROM scratch"), 0644)
			require.NoError(t, err)
			summary, err := docker.BuildDryRun()
			assert.NoError(t, err)

			assert.Contains(t, summary, "FROM scratch")
		})
	})

	t.Run("with nested builder", func(t *testing.T) {
		builder := new(MockBuilder)
		builder.On("BuildDryRun").Return("dry-run-summary", nil).Once()

		fs.WithTestFs(func() {
			docker := newDocker()
			docker.Before = []Builder{builder}

			summary, err := docker.BuildDryRun()
			require.NoError(t, err)

			assert.Contains(t, summary, "dry-run-summary")
		})
	})

	t.Run("with nested builder returning an error", func(t *testing.T) {
		builder := new(MockBuilder)
		builder.On("BuildDryRun").Return("dry-run-summary", errors.New("nested error")).Once()

		fs.WithTestFs(func() {
			docker := newDocker()
			docker.Before = []Builder{builder}

			summary, err := docker.BuildDryRun()

			assert.Equal(t, "nested error", err.Error())
			assert.Contains(t, summary, "nested docker before builder error")
		})
	})
}
