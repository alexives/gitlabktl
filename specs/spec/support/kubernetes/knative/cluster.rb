require 'kubeclient'

module Kubernetes
  module Knative
    class Cluster
      def initialize(config)
        @client = Kubeclient::Client
          .new(config.knative_endpoint, 'v1alpha1', config.to_options)

        @config = config
      end

      def services
        @services ||= namespace_services.map do |resource|
          Knative::Service.new(resource)
        end
      end

      def self.fabricate!
        @instance ||= new(Kubernetes::Config.build!)
      end

      private

      def namespace_services
        @client.get_services(namespace: @config.namespace)
      end
    end
  end
end
