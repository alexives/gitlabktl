require 'spec_helper'
require_relative 'functions_shared_examples'

RSpec.describe 'Function using the GitLab Ruby runtime' do
  include_examples 'serverless functions deployment', 'func-echo'
end
