package runtime

import (
	"fmt"
	"os"
	"path"

	"gopkg.in/src-d/go-billy.v4"
	"gopkg.in/src-d/go-billy.v4/memfs"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

type Client interface {
	WriteFiles(directory string) error
	ReadFile(path string) (string, error)
	ListFiles(path string) ([]os.FileInfo, error)
}

type GoGit struct {
	Location
	*git.Repository
	billy.Filesystem
}

func NewClient(location Location) (Client, error) {
	filesystem := memfs.New()

	repository, err := git.Clone(memory.NewStorage(), filesystem, &git.CloneOptions{
		URL: location.Address,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to clone repository: %w", err)
	}

	worktree, err := repository.Worktree()
	if err != nil {
		return nil, fmt.Errorf("could not fetch the repository worktree: %w", err)
	}

	err = worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.NewBranchReferenceName(location.Reference),
	})
	if err != nil {
		return nil, fmt.Errorf("could not checkout the runtime version: %w", err)
	}

	return &GoGit{Location: location, Repository: repository, Filesystem: filesystem}, nil
}

func (client *GoGit) WriteFiles(directory string) error {
	commit, err := client.headCommit()
	if err != nil {
		return fmt.Errorf("could not find HEAD commit: %w", err)
	}

	iterator, err := commit.Files()
	if err != nil {
		return fmt.Errorf("could not find requested file: %w", err)
	}

	return iterator.ForEach(func(file *object.File) error {
		path := path.Join(directory, file.Name)

		err := fs.MkdirAllPath(path)
		if err != nil {
			return err
		}

		contents, err := file.Contents()
		if err != nil {
			return err
		}

		return fs.WriteFile(path, []byte(contents), 0644)
	})
}

func (client *GoGit) ReadFile(path string) (string, error) {
	commit, err := client.headCommit()
	if err != nil {
		return "", fmt.Errorf("could not find HEAD commit: %w", err)
	}

	file, err := commit.File(path)
	if err != nil {
		return "", fmt.Errorf("could not find requested file: %w", err)
	}

	contents, err := file.Contents()
	if err != nil {
		return "", fmt.Errorf("could not read requested file contents: %w", err)
	}

	return contents, nil
}

func (client *GoGit) ListFiles(path string) ([]os.FileInfo, error) {
	return client.ReadDir(path)
}

func (client *GoGit) headCommit() (*object.Commit, error) {
	ref, err := client.Head()
	if err != nil {
		return new(object.Commit), fmt.Errorf("could not fetch repository HEAD: %w", err)
	}

	commit, err := client.CommitObject(ref.Hash())
	if err != nil {
		return new(object.Commit), fmt.Errorf("could not find HEAD commit: %w", err)
	}

	return commit, nil
}
