package runtime

import (
	"bytes"
	"errors"

	engine "text/template"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/logger"
)

type Template struct {
	Filename   string
	Contents   string
	Attributes Details
}

// Parse a template, and return a string
func (template Template) Parse() (string, error) {
	logger.WithField("filename", template.Filename).Info("Parsing the template")

	parser := engine.Must(engine.New(template.Filename).Parse(template.Contents))
	buffer := new(bytes.Buffer)
	err := parser.Execute(buffer, template.Attributes)

	return buffer.String(), err
}

// Write parsed template to a file on disk
func (template Template) Write() error {
	if exists, _ := fs.Exists(template.Filename); exists {
		return errors.New("template output file already exists")
	}

	contents, err := template.Parse()
	if err != nil {
		return err
	}

	return fs.WriteFile(template.Filename, []byte(contents), 0644)
}
