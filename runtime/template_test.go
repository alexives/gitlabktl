package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestTemplateDockerfile(t *testing.T) {
	template := Template{
		Attributes: Details{
			FunctionName:    "ruby-function",
			FunctionRuntime: "serverless/runtimes/ruby",
		},
		Filename: "my-template",
		Contents: "FROM {{.FunctionName}}",
	}

	contents, err := template.Parse()
	require.NoError(t, err)

	assert.Equal(t, "FROM ruby-function", contents)
}

func TestTemplateWrite(t *testing.T) {
	fs.WithTestFs(func() {
		template := Template{
			Attributes: Details{
				FunctionName: "ruby-function",
			},
			Filename: "my/template",
			Contents: "Some {{.FunctionName}}",
		}

		template.Write()

		output, err := fs.ReadFile("my/template")
		require.NoError(t, err)

		assert.Equal(t, "Some ruby-function", string(output))
	})
}
