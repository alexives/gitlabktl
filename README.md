# GitLab Knative tool

`gitlabktl` is a tool that makes it easier to build and deploy your serverless
applications and functions to Knative using GitLab CI/CD.

It uses:

* [Kaniko](https://github.com/GoogleContainerTools/kaniko) to build functions
* GitLab Container Registry / custom registry to store images
* [tm](https://github.com/triggermesh/tm) tool to deploy services


## Usage

See `gitlabktl --help`

1. Bulding a function runtime

    ```
    gitlabktl function build --help
    ```

1. Build functions defined in a serverless configuration file

    ```
    gitlabktl serverless build --help
    ```

1. Deploy functions defined in a serverless configuration file

    ```
    gitlabktl serverless deploy --help
    ```

1. Build a serverless application using a `Dockerfile`

    ```
    gitlabktl app build --help
    ```

### Building and running functions locally

You can use `gitlabktl` to build functions using Docker Engine and to run them
locally. Take a look at
[documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/#running-functions-locally)
on docs.gitlab.com.

## Development

See [DEVELOPMENT.md](DEVELOPMENT.md)

## State

Alpha

## License

MIT
