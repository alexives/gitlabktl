package env

import (
	"os"
)

var env Env

// Helper method for quickly accessing environment variables
func Getenv(key string) string {
	return New().Getenv(key)
}

// Helper method for quickly check if envs are present
func Defined(keys ...string) bool {
	for _, key := range keys {
		_, ok := New().LookupEnv(key)

		if !ok {
			return false
		}
	}

	return true
}

// Env interface describes env objects used for environment variables lookup
type Env interface {
	Getenv(key string) string
	LookupEnv(key string) (string, bool)
}

func setDefaultEnv() {
	env = new(osEnv)
}

type osEnv struct{}

func (env osEnv) Getenv(key string) string {
	return os.Getenv(key)
}

func (env osEnv) LookupEnv(key string) (string, bool) {
	return os.LookupEnv(key)
}

// Stubs that allow you to define key -> value of an environment variable
type Stubs map[string]string

type stubbedEnv struct {
	values Stubs
}

func (env stubbedEnv) Getenv(key string) string {
	return env.values[key]
}

func (env stubbedEnv) LookupEnv(key string) (string, bool) {
	value, ok := env.values[key]

	return value, ok
}

// Create a new environment accessor
func New() Env {
	if env == nil {
		setDefaultEnv()
	}

	return env
}

// Used mostly in tests, allows you to stub environment for testing
func WithStubbedEnv(stubs Stubs, block func()) {
	env = stubbedEnv{values: stubs}
	defer setDefaultEnv()

	block()
}
