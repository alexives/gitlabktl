package commands

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/logger"
)

var contextCtx context.Context

var defaultContext = func() context.Context {
	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGQUIT, os.Interrupt)

	ctx, cancelFunc := context.WithCancel(context.Background())
	go func() {
		sig := <-signals
		logger.Warnf("Received %q signal; quitting", sig)
		cancelFunc()
	}()

	return ctx
}

// CLI action function invocation context
type Context struct {
	Cli *cli.Context
	Ctx context.Context
	env.Env
}

func newContext(cliCtx *cli.Context) *Context {
	if contextCtx == nil {
		contextCtx = defaultContext()
	}

	return &Context{Cli: cliCtx, Ctx: contextCtx, Env: env.New()}
}
