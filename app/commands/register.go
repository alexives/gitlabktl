package commands

import (
	"github.com/urfave/cli"
	clihelpers "gitlab.com/ayufan/golang-cli-helpers"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

// Commands register
type Register struct {
	commands []Registrable
}

// Adds a new command to the Register
func (register *Register) RegisterCommand(command Command) {
	logger.Debugln("Registering", command.Path, "command")

	command.Flags = clihelpers.GetFlagsFromStruct(command.Handler)
	command.Action = command.toActionFunc()

	register.commands = append(register.commands, command.Registrable)
}

// Adds a new category to the Register
func (register *Register) RegisterCategory(category Category) {
	logger.Debugln("Registering", category.Path, "category")

	register.commands = append(register.commands, category.Registrable)
}

// Returns all registered categories and commands as `cli.Command` tree
func (register *Register) GetCommands() []cli.Command {
	var cmds []cli.Command
	var compose func(cmd Registrable) cli.Command

	compose = func(cmd Registrable) cli.Command {
		for _, command := range register.commands {
			if command.parentPath() == cmd.Path {
				cmd.Subcommands = append(cmd.Subcommands, compose(command))
			}
		}

		return cli.Command(cmd.Config)
	}

	for _, cmd := range register.commands {
		if cmd.root() {
			cmds = append(cmds, compose(cmd))
		}
	}

	return cmds
}

func NewRegister() *Register {
	return new(Register)
}
