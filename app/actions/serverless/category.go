package serverless

import (
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
)

func newServerlessCategory() commands.Category {
	return commands.Category{
		Registrable: commands.Registrable{
			Path: "serverless",
			Config: commands.Config{
				Name:        "serverless",
				Aliases:     []string{"sl"},
				Usage:       "Manage your serverless environment",
				Description: "Manage your serverless environment",
			},
		},
	}
}

func RegisterCategory(register *commands.Register) {
	register.RegisterCategory(newServerlessCategory())
}
