package serverless

import (
	"bytes"
	"os"
	"testing"

	"github.com/MakeNowJust/heredoc"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/knative"
)

func TestServerlessDryRunDeployment(t *testing.T) {
	knative.WithMockClient(&knative.MockClient{Compatible: true}, func(client *knative.MockClient) {
		buffer := new(bytes.Buffer)
		service.Output = buffer
		defer func() { service.Output = os.Stdout }()

		action := DeployAction{
			DryRun:         true,
			KubeConfigFile: "../../../k8s/testdata/kubeconfig.json",
			ServerlessFile: "../../../serverless/testdata/serverless.yml",
		}

		envs := env.Stubs{
			"CI_REGISTRY":           "my.registry",
			"CI_PROJECT_VISIBILITY": "public",
		}

		env.WithStubbedEnv(envs, func() {
			err := action.Execute(new(commands.Context))
			require.NoError(t, err)
		})

		output := buffer.String()
		assert.Contains(t, output, `"kind": "Service"`)
		assert.Contains(t, output, `"apiVersion": "serving.knative.dev/v1alpha1"`)
		assert.Contains(t, output, `"image": "registry.gitlab.com/my-project/my-functions-echo"`)
		assert.Contains(t, output, `"namespace": "testNamespace"`)
		assert.Contains(t, output, `"name": "my-k8s-secret"`)
	})
}

func TestServerlessDeployment(t *testing.T) {
	action := DeployAction{
		ServerlessFile: "../../../serverless/testdata/serverless.yml",
		KubeConfigFile: "../../../k8s/testdata/kubeconfig.json",
	}

	t.Run("when project is public", func(t *testing.T) {
		envs := env.Stubs{
			"CI_REGISTRY":           "my.registry",
			"CI_PROJECT_VISIBILITY": "public",
		}

		knative.WithMockCluster(func(cluster *knative.MockCluster) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("DeployServices", mock.Anything).Return("ok", nil).Once()
			defer cluster.AssertExpectations(t)

			env.WithStubbedEnv(envs, func() {
				assert.NoError(t, action.Execute(new(commands.Context)))
			})
		})
	})

	t.Run("when project is private", func(t *testing.T) {
		envs := env.Stubs{
			"CI_PROJECT_VISIBILITY": "private",
			"CI_REGISTRY":           "my.registry",
			"CI_DEPLOY_USER":        "user",
			"CI_DEPLOY_PASSWORD":    "pass",
		}

		knative.WithMockCluster(func(cluster *knative.MockCluster) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("DeployRegistryCredentials").Return(nil).Once()
			cluster.On("DeployServices", mock.Anything).
				Return("deployed", nil).Once()
			defer cluster.AssertExpectations(t)

			env.WithStubbedEnv(envs, func() {
				assert.NoError(t, action.Execute(new(commands.Context)))
			})
		})
	})
}

func TestManifest(t *testing.T) {
	t.Run("with valid serverless manifest", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := heredoc.Doc(`
				service: test-service
				provider:
				  name: triggermesh

				functions:
				  echo:
				    handler: Echo.run
			`)

			err := fs.WriteFile("my-serverless.yaml", []byte(yaml), 0644)
			require.NoError(t, err)

			action := DeployAction{ServerlessFile: "my-serverless.yaml"}
			manifest, err := action.Manifest()

			assert.NoError(t, err)
			assert.Equal(t, "test-service", manifest.Service)
		})
	})

	t.Run("with invalad serverless manifest", func(t *testing.T) {
		fs.WithTestFs(func() {
			yaml := "service: test-service"

			err := fs.WriteFile("my-serverless.yaml", []byte(yaml), 0644)
			require.NoError(t, err)

			action := DeployAction{ServerlessFile: "my-serverless.yaml"}
			_, err = action.Manifest()

			assert.EqualError(t, err, "invalid provider: provider name can not be empty")
		})
	})
}
