package application

import (
	"bytes"
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/knative"
)

func TestDefaultSourceImage(t *testing.T) {
	action := deployAction{}

	env.WithStubbedEnv(env.Stubs{"CI_REGISTRY_IMAGE": "my.registry/project"}, func() {
		assert.Equal(t, "my.registry/project:latest", action.ApplicationSource())
	})
}

func TestDefaultApplicationName(t *testing.T) {
	action := deployAction{}

	env.WithStubbedEnv(env.Stubs{"CI_PROJECT_NAME": "my-project"}, func() {
		assert.Equal(t, "my-project", action.ApplicationName())
	})
}

func TestDefaultNamespace(t *testing.T) {
	action := deployAction{}

	env.WithStubbedEnv(env.Stubs{"KUBE_NAMESPACE": "my-namespace"}, func() {
		assert.Equal(t, "my-namespace", action.Namespace())
	})
}

func TestAppDeployDryRun(t *testing.T) {
	knative.WithMockClient(&knative.MockClient{Compatible: true}, func(client *knative.MockClient) {
		buffer := new(bytes.Buffer)
		output = buffer
		defer func() { output = os.Stdout }()

		action := deployAction{
			Name:           "my-app",
			Image:          "my.registry/my/image",
			KubeConfigFile: "../../../k8s/testdata/kubeconfig.json",
			DryRun:         true,
			Secrets:        []string{"my-secret"},
			Envs:           map[string]string{"my-env": "my-env-value"},
		}

		err := action.Execute(new(commands.Context))
		require.NoError(t, err)
		message := buffer.String()

		assert.Contains(t, message, `"kind": "Service"`)
		assert.Contains(t, message, `"apiVersion": "serving.knative.dev/v1alpha1"`)
		assert.Contains(t, message, `"image": "my.registry/my/image:latest"`)
		assert.Contains(t, message, `"name": "my-app"`)
		assert.Contains(t, message, `"name": "my-secret"`)
		assert.Contains(t, message, `"name": "my-env"`)
		assert.Contains(t, message, `"value": "my-env-value"`)
		assert.Contains(t, message, `"namespace": "testNamespace"`)
	})
}

func TestAppDeploy(t *testing.T) {
	action := deployAction{
		Name:  "my-app",
		Image: "my.registry/my/image",
	}

	knative.WithMockCluster(func(cluster *knative.MockCluster) {
		t.Run("when deployment failed", func(t *testing.T) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("NeedsRegistryCredentials").Return(false).Once()
			cluster.On("DeployService", mock.Anything).
				Return("error", errors.New("deployment error")).Once()

			err := action.Execute(new(commands.Context))

			assert.Equal(t, "could not deploy application: deployment error", err.Error())
		})

		t.Run("when deployment succeeded", func(t *testing.T) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("NeedsRegistryCredentials").Return(false).Once()
			cluster.On("DeployService", mock.Anything).
				Return("deployed to cluster", nil).Once()

			err := action.Execute(new(commands.Context))

			assert.NoError(t, err)
		})
	})
}
