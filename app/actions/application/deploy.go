package application

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/k8s"
	"gitlab.com/gitlab-org/gitlabktl/knative"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

var output io.Writer = os.Stdout

type deployAction struct {
	Name           string            `long:"name" env:"GITLAB_APP_NAME" description:"Application name"`
	Image          string            `long:"image" env:"GITLAB_APP_IMAGE" description:"Source image repository"`
	Tag            string            `long:"tag" env:"GITLAB_APP_TAG" description:"Source image tag"`
	Username       string            `long:"registry-username" env:"GITLAB_REGISTRY_USERNAME" description:"Registry username"`
	Password       string            `long:"registry-password" env:"GITLAB_REGISTRY_PASSWORD" description:"Registry password"`
	KubeConfigFile string            `short:"c" long:"kubeconfig" env:"GITLAB_KUBECONFIG_FILE" description:"Path to kubeconfig"`
	DryRun         bool              `short:"t" long:"dry-run" env:"GITLAB_APP_DRYRUN" description:"Dry run only"`
	Secrets        []string          `long:"secret" description:"Kubernetes secrets"`
	Envs           map[string]string `long:"env" description:"Application environment variables"`
}

func (action *deployAction) Execute(context *commands.Context) error {
	service := knative.Service{
		Name:      action.ApplicationName(),
		Image:     action.ApplicationSource(),
		Namespace: action.Namespace(),
		Secrets:   action.Secrets,
		Envs:      action.Envs,
	}

	deployer, err := knative.NewApplicationDeployer(service, action)
	if err != nil {
		return fmt.Errorf("could not create a new application deployer: %w", err)
	}

	var message string
	if action.DryRun {
		message, err = deployer.DeployDryRun()
	} else {
		message, err = deployer.Deploy(context.Ctx)
	}

	if err != nil {
		return fmt.Errorf("could not deploy application: %w", err)
	}

	if err == nil {
		fmt.Fprint(output, message)
	}

	return err
}

func (action *deployAction) ToImage() registry.Image {
	return registry.Image{
		Repository: action.Image,
		Tag:        action.Tag,
		Username:   action.Username,
		Password:   action.Password,
	}
}

func (action *deployAction) ApplicationSource() string {
	return action.ToImage().ToString()
}

func (action *deployAction) ApplicationName() string {
	if len(action.Name) > 0 {
		return action.Name
	}

	return env.Getenv("CI_PROJECT_NAME")
}

func (action *deployAction) Namespace() string {
	return env.Getenv("KUBE_NAMESPACE")
}

func (action *deployAction) Registry() knative.Registry {
	image := action.ToImage()
	source := image.PullRegistry()

	return knative.Registry{
		Host:       source.Host,
		Username:   source.Username,
		Password:   source.Password,
		Repository: image.ToRepository(),
	}
}

func (action *deployAction) KubeConfig() string {
	return k8s.NewKubeConfigPath(action.KubeConfigFile)
}

func newDeployCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "application/deploy",
			Config: commands.Config{
				Name:        "deploy",
				Aliases:     []string{},
				Usage:       "Deploy your serverless application",
				Description: "This commands deploys an image of you serverless application",
			},
		},
		Handler: new(deployAction),
	}
}

func RegisterDeployCommand(register *commands.Register) {
	register.RegisterCommand(newDeployCommand())
}
