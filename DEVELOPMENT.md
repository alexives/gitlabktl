# Development

This file describes processes around `gitlabktl` development.

## Prerequisites

- Go 1.13 or newer

## Development environment setup

- Clone this repository
- Clone submodules: `git submodule update --init --recursive`

## Unit tests

Run `make test` or `go test ./...` in a root directory.

## Linting

Run `make lint`. This will check code quality in codeclimate using an extra
engine for gocyclo.

## End-to-end tests

End-to-end tests are configured to run in the CI/CD environment. End-to-end
tests run by default when:

1. A merge request gets merged into the `master` branch.
1. A new Git tag gets pushed to the repository.
1. A merge request author requests running end-to-end tests on demand.

### Running end-to-end tests on demand

Whenever you want to run a full CI/CD pipeline with end-to-end tests you need
to create and push a commit with `[gitlabktl end-to-end]` text in the commit
message.

### How do we end-to-end test `gitlabktl`?

1. Pipeline runs unit tests
1. Pipeline builds `gitlabktl` binary and stores it as an artifact
1. Pipeline builds a new Docker image a tags it with Git commit SHA
1. The Docker image gets pushed to the Container Registry
1. We use the new Docker image to build a test function and test service
1. We use the new Docker image to deploy them to a real Knative cluster
1. We run Rspec test suite to validate deployments and functions
